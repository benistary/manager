import React from 'react';
import { View } from 'react-native';
import Core from './src/Core';

export default class App extends React.Component {
  render() {
    return (
      <View>
          <Core />
      </View>
    );
  }
}
