import React from 'react';
import { connect } from 'redux';
import { emailChanged } from '../actions';
import { Card, CardSection, Input, Button } from './common';

class LoginForm extends React.Component {

    onEmailChanged(text) {
        this.props.emailChanged(text);
    }

    render() {
        return(
            <Card>
                <CardSection>
                    <Input
                        value={this.props.email}
                        label="Email"
                        placeholder="email@gmail.com"
                        onChangeText={this.onEmailChanged.bind(this)}
                    />
                </CardSection>
                <CardSection>
                    <Input
                        secureTextEntry
                        label="Password"
                        placeholder="password"
                    />
                </CardSection>
                <CardSection>
                    <Button>
                        Log in
                    </Button>
                </CardSection>
            </Card>
        );
    }
}

const mapStateToProps = state => {
    return {
        email: state.auth.email
    };
};

const mapDispatchToProps = dispatch => {
    return {
    //Ak tam cches dat nejake parametre tak ich musis dat aj do tych prvych aj druhych zatvoriek.
        emailChanged: () => dispatch(emailChanged())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginForm);
